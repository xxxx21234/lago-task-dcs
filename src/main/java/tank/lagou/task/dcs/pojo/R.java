package tank.lagou.task.dcs.pojo;

public class R<T> {

    private final static Integer SUCCESS_CODE = 200;

    private Integer code;
    private String message;
    private String error;
    private T body;

    public R(Integer code, String message, String error, T body) {
        this.code = code;
        this.message = message;
        this.error = error;
        this.body = body;
    }

    private static <T> R createR(Integer code, String message, String error, T body){
        return new R(code,message,error,body);
    }

    public static <T> R success(T body){
        return createR(SUCCESS_CODE,null,null,body);
    }

    public static R success(String message){
        return createR(SUCCESS_CODE,message,null,null);
    }

    public R success(){
        return createR(SUCCESS_CODE,null,null,null);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
