package tank.lagou.task.dcs.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

public class PasswordEncorder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        System.out.println(rawPassword + ":" + encodedPassword);
        return encodedPassword.contains(rawPassword);
        //return rawPassword.toString().equals(encodedPassword);
    }
}
