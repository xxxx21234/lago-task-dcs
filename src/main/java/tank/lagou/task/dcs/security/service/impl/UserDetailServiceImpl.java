package tank.lagou.task.dcs.security.service.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import tank.lagou.task.dcs.security.pojo.UserDetailsPojo;

import java.util.ArrayList;
import java.util.List;
@Component
public class UserDetailServiceImpl implements UserDetailsService {
    private static final List<UserDetails> users = new ArrayList<>();

    static {
        UserDetailsPojo userDetails = new UserDetailsPojo();
        userDetails.setUsername("tank");
        userDetails.setPassword("123456");
        users.add(userDetails);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("load username:" + username);

        /*UserDetailsPojo userDetails = new UserDetailsPojo();
        userDetails.setUsername(username);
        userDetails.setPassword("123456");*/
        for (UserDetails user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }
}
