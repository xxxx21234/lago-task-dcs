package tank.lagou.task.dcs.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//@WebFilter
public class LogHandlerInterceptor implements HandlerInterceptor {
    private final static Logger logger = LoggerFactory.getLogger(LogHandlerInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession httpSession = request.getSession(false);
        if(httpSession != null){
            logger.info(request.getRequestURI() + " this session is :{} and port is : {},",httpSession.getId(),request.getServerPort());
            //System.out.println(String.format("this session is :{} and port is : {},",httpSession.getId(),request.getServerPort()));
        }else{
            logger.info(request.getRequestURI() + "httpsession is null!!! and port is : {}",request.getServerPort());
            //System.out.println(String.format("httpsession is null!!! and port is : {}",request.getServerPort()));
        }

        return true;
    }
}
